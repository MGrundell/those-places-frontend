var express = require('express');
var router = express.Router();
/* GET home page. */
var 	Client = require('node-rest-client').Client;
var 	client = new Client();
var   session = require('client-sessions');


router.get('/', function(req, res, next) {
  	// parsed response body as js object
      client.registerMethod("jsonMethod", "http://ec2-54-154-0-13.eu-west-1.compute.amazonaws.com:9000/board-service/boards", "GET");
      var req2 = client.methods.jsonMethod(function (data, response) {
    	res.render('index.jade', { boards: data });
      });
});

router.post('/', function(req, res, rext){
  var searchBoard=req.body.searchBoard;
  //URL SAFE encoder
   var testing = searchBoard.replace(/ /g, "&20;");
  client.registerMethod("jsonMethod", "http://ec2-54-154-0-13.eu-west-1.compute.amazonaws.com:9000/board-service/search?query="+ testing +"", "GET");
    var req2 = client.methods.jsonMethod(function (data, response) {
    // parsed response body as js object
    res.render('index.jade', { boards: data,});
  });
});

module.exports = router;
