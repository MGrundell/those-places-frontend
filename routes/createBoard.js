var express = require('express');
var router = express.Router();
/* GET home page. */
var 	Client = require('node-rest-client').Client;
var 	client = new Client();
var bodyParser = require('body-parser');
var   functions = require('../functions.js');

router.get('/', function(req, res, next) {
  client.registerMethod("jsonMethod", "http://ec2-54-154-0-13.eu-west-1.compute.amazonaws.com:9000/board-service/boards", "GET");
  var req2 = client.methods.jsonMethod(function (data, response) {
    // parsed response body as js object
    res.render('index.jade', { boards: data });
  });
});

router.post('/', functions.requireLogin, function(req, res, rext){
  var boardShortName = req.body.name;
  var boardDescription = req.body.description;
  var mainImageLink = req.body.imageLink;
  var mainHotelLink = "HOTEL";

  var args = {
		parameters: {
			  boardShortName: boardShortName,
        boardDescription: boardDescription,
        mainImageLink:  mainImageLink,
        mainHotelLink: mainHotelLink,
        createdBy:  req.session.user.email,
		}
	};
  client.registerMethod("postMethod", "http://ec2-54-154-0-13.eu-west-1.compute.amazonaws.com:9000/board-service", "POST" );
	client.methods.postMethod(args, function(data, response){
      res.render('createdBoard.jade', { boards: data });
	});
});

module.exports = router;
