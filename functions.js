// Require Login To display Page.
var requireLogin = function(req, res, next ){
  if(!req.user){
    res.redirect('/login');
  }else{
    next();
  }
};

exports.requireLogin = requireLogin;
