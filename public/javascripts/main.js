(function($) {
    "use strict"; // Start of use strict
    // masonry
    $(document).ready(function(){
          $( function() {

            var $container = $('.msnry').masonry({
                itemSelector: '.msnry-item',
                columnWidth: '.msnry-sizer',
                percentPosition: true,
                gutter: 10
            });

            // reveal initial images
            $container.masonryImagesReveal( $('.board-items').find('.msnry-item') );

          });

          $.fn.masonryImagesReveal = function( $items ) {
            var msnry = this.data('masonry');
            var itemSelector = msnry.options.itemSelector;
            // hide by default
            $items.hide();
            // append to container
            this.append( $items );
            $items.imagesLoaded().progress( function( imgLoad, image ) {
              // get item
              // image is imagesLoaded class, not <img>, <img> is image.img
              var $item = $( image.img ).parents( itemSelector );
              // un-hide item
             $item.show();
              // masonry does its thing
              msnry.appended( $item );
            });

            return this;
          };


        });
    //Semantic
    $(document).ready(function(){
      $('.special.cards .image').dimmer({
        on: 'hover'
      });

      $('.ui.dropdown').dropdown();

      $('.createBoardBTN').click(function(){
        $('.ui.basic.modal.createBoard').modal('show');
      });

      $('.singUpBtn').click(function() {
        $('.ui.basic.modal.signup').modal('show');
      });

      $('.loginBTN').click(function() {
        $('.ui.basic.modal.login').modal('show');
      });

      $('.ui.modal.standard.visible.active')
        .modal({
          inverted: true
        })
        .modal('show');

        $('.button.myPopup').popup({ on: 'click' });

        $('button.fallow')
          .api({
            on: 'click',
            method: "POST",
            url: 'http://ec2-54-154-0-13.eu-west-1.compute.amazonaws.com:9000/user-service/followUser?email={id}&followEmail={user}',
          })
        ;

      // Signup Validation
      $('.ui.form.signup').form({
        fields: {
          firstname : 'empty',
          lastname    : 'empty',
          homeCity  : 'empty',
          password  : ['minLength[6]', 'empty'],
          homeCountry:  'empty',
          gender     : 'empty'
        }
      });

    });

})(jQuery); // End of use strict
